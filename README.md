SPVIEW
=====

> Copyright &copy; < 2015, Christian Wenger

> Copyright &copy; 2015-2021, Cyril Richard
> <<https://icb.u-bourgogne.fr/programmes-et-banques-de-donnees-spectroscopiques/>>

Summary
-------
SPVIEW is a multiplatform Java application that allows graphical assignment of
high-resolution molecular spectra. It is possible to load, display and manipulate
experimental and simulated spectra (XY ASCII or [OPUS](https://www.bruker.com/en/products-and-solutions/infrared-and-raman/opus-spectroscopy-software.html) formats) as well as stick spectra in
various formats (including HITRAN format). Lines can be assigned graphically using
the mouse. Assignments can also be modified or removed. Local simulations can be
performed in order, for instance, to help assignment in partly resolved line
clusters. SPVIEW is also able to produce peak lists from an experimental spectrum. 

Contributors are welcome. Programming language is java.

Source download
---------------

You can get SPVIEW source code from the release archives on the latest version from git:

    git clone https://gitlab.com/labicb/spview.git

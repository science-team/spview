package org.spview;

/*
 * SPVIEW entry
 */

import org.spview.gui.JobPlay;
import org.spview.preferences.WindowHandler;

import java.util.Locale;


/**
 * This is the entry point to SPVIEW application.
 */
public class Spview {

	public final static String PACKAGE_NAME = "SPVIEW";
	public final static String VERSION = "2.0.2";

	/**
	 * Start SPVIEW application.
	 */
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(() -> {
			Locale.setDefault(Locale.ENGLISH); // messages in english
			WindowHandler.setLookAndFeel();
			JobPlay jp = new JobPlay(); // instantiate the JobPlay main window
			WindowHandler.registerFrame(jp, Spview.class.getName(), 0, 0, 1024, 768);
			jp.setVisible(true); // show it
			if (args.length > 0) {
				jp.loadProjectFileName(args[0]);
			}
		});
	}
}

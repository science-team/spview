package org.spview.filehandler;

import java.awt.geom.Point2D;

public class TDSFile {

	public static Point2D extractXY(String str) {
		double cx, cy;
		// TDS pred-as-stick
		// check line length
		if ((str.length() != 97) && // TDS with population
				(str.length() != 101) && // C3vsTDS with population
				(str.length() != 92)) { // D2hStark
			// length error
			return null; // skip it
		}
		//
		if (str.length() == 92) {
			// D2hStark
			if (str.charAt(18) != ' ' || str.charAt(28) != ' '
					|| str.charAt(32) != ' ' || str.charAt(36) != ' '
					|| str.charAt(40) != ' ' || str.charAt(44) != ' '
					|| str.charAt(48) != ' ' || str.charAt(52) != ' '
					|| str.charAt(56) != ' ' || str.charAt(60) != ' '
					|| str.charAt(76) != ' ' || str.charAt(80) != ' '
					|| str.charAt(84) != ' ' || str.charAt(88) != ' ') { // format error
				return null; // skip the line
			}
			try {
				// read data
				cx = Double.parseDouble(str.substring(0, 18)); // Double for X
				cy = Double.parseDouble(str.substring(19, 28)); // Double for Y

				Integer.parseInt(str.substring(29, 32).trim()); // validity test
				Integer.parseInt(str.substring(33, 36).trim()); // validity test
				Integer.parseInt(str.substring(37, 40).trim()); // validity test
				Integer.parseInt(str.substring(41, 44).trim()); // validity test
				Integer.parseInt(str.substring(45, 48).trim()); // validity test
				Integer.parseInt(str.substring(49, 52).trim()); // validity test
				Integer.parseInt(str.substring(53, 56).trim()); // validity test
				Integer.parseInt(str.substring(57, 60).trim()); // validity test

				Double.valueOf(str.substring(61, 76)); // validity test

				Integer.parseInt(str.substring(77, 80).trim()); // validity test
				Integer.parseInt(str.substring(81, 84).trim()); // validity test
				Integer.parseInt(str.substring(85, 88).trim()); // validity test
				Integer.parseInt(str.substring(89, 92).trim()); // validity test
			} catch (NumberFormatException e) { // format error
				return null; // skip the line
			}
		} else if (str.length() == 101) {
			// C3vsTDS
			if (!str.startsWith("  ", 24) || str.charAt(33) != ' '
					|| !str.startsWith("% ", 47) || str.charAt(54) != ' '
					|| !str.startsWith("% ", 68) || str.charAt(85) != ' ') { // format error
				return null; // skip the line
			}
			try {
				// read data
				cx = Double.parseDouble(str.substring(0, 15)); // Double for X
				cy = Double.parseDouble(str.substring(15, 24)); // Double for Y

				Double.valueOf(str.substring(28, 33).trim()); // validity test

				Integer.parseInt(str.substring(36, 40).trim()); // validity test
				Integer.parseInt(str.substring(40, 43).trim()); // validity test
				Integer.parseInt(str.substring(43, 47).trim()); // validity test

				Double.valueOf(str.substring(49, 54).trim()); // validity test

				Integer.parseInt(str.substring(58, 61).trim()); // validity test
				Integer.parseInt(str.substring(61, 64).trim()); // validity test
				Integer.parseInt(str.substring(64, 68).trim()); // validity test

				Double.valueOf(str.substring(70, 85)); // validity test
				Double.valueOf(str.substring(86, 101)); // validity test
			} catch (NumberFormatException e) { // format error
				return null; // skip the line
			}
		} else {
			// TDS (97)
			if (!str.startsWith("  ", 24) || str.charAt(31) != ' '
					|| !str.startsWith("% ", 45) || str.charAt(50) != ' '
					|| !str.startsWith("% ", 64) || str.charAt(81) != ' ') { // format error
				return null; // skip the line
			}
			try {
				// read data
				cx = Double.parseDouble(str.substring(0, 15)); // Double for X
				cy = Double.parseDouble(str.substring(15, 24)); // Double for Y

				Integer.parseInt(str.substring(28, 31).trim()); // validity test
				Integer.parseInt(str.substring(35, 38).trim()); // validity test
				Integer.parseInt(str.substring(38, 41).trim()); // validity test
				Integer.parseInt(str.substring(41, 45).trim()); // validity test
				Integer.parseInt(str.substring(47, 50).trim()); // validity test
				Integer.parseInt(str.substring(54, 57).trim()); // validity test
				Integer.parseInt(str.substring(57, 60).trim()); // validity test
				Integer.parseInt(str.substring(60, 64).trim()); // validity test

				Double.valueOf(str.substring(66, 81)); // validity test
				Double.valueOf(str.substring(82, 97)); // validity test
			} catch (NumberFormatException e) { // format error
				return null; // skip the line
			}
		}
		return (new Point2D.Double(cx, cy));
	}

	public static String extractAssignment(String str) {
		String cjsyn;
		if (str.length() == 101) {
			// C3vsTDS
			cjsyn = str.substring(28, 40) + " " + str.substring(49, 61);
		} else {
			// TDS (97)
			cjsyn = str.substring(28, 38) + " " + str.substring(47, 57);
		}
		return cjsyn;
	}
}
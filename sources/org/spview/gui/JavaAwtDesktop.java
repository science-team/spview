package org.spview.gui;

import java.awt.Desktop;

public class JavaAwtDesktop {

    public JavaAwtDesktop() {
        Desktop desktop = Desktop.getDesktop();

        if (desktop.isSupported(Desktop.Action.APP_ABOUT)) {
            desktop.setAboutHandler(e -> About.show());
        }
    }
}

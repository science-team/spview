package org.spview.point;

import org.spview.filehandler.FortranFormat;

public class ResidualPoint {
	private final double x;
	private final double y;
	private final String attrib;

	public ResidualPoint(double x, double y, String attribution) {
		this.x = x;
		this.y = y;
		this.attrib = attribution;
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public String getAttrib() {
		return this.attrib;
	}

	public static double distanceSq(double x1, double y1, double x2, double y2) {
		x2 -= x1;
		y2 -= y1;
		return x2 * x2 + y2 * y2;
	}

	public static double distance(double x1, double y1, double x2, double y2) {
		return Math.sqrt(distanceSq(x1, y1, x2, y2));
	}

	public double distance(double x, double y) {
		return distance(getX(), getY(), x, y);
	}
	
	public String toString() {
		String lnsep = System.getProperty("line.separator");

		String strX = FortranFormat.formFreq(getX());
		String strY = FortranFormat.formResidus(getY());
		return "x=" + strX + ", delta=" + strY + " ass=" + getAttrib() + lnsep;
	}

}
